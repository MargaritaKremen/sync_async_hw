import asyncio
import logging
import aiohttp

logging.basicConfig(filename="log_for_async.txt", level=logging.INFO)


async def fetch_url(url, logger):
    async with aiohttp.ClientSession() as session:
        logger.info(f"Starting reguest for {url}")
        async with session.get(url) as response:
            if response.status == 200:
                logger.info(f"Received response 200 for {url}")
            else:
                logger.warning(f"Received response {response.status} for {url}")


async def main():
    logger = logging.getLogger(__name__)
    urls = [
        "http://www.testingmcafeesites.com/index.html",
        "https://github.com/",
        "https://www.jython.org",
        "https://google.com",
    ]
    tasks = [fetch_url(url, logger) for url in urls]
    await asyncio.gather(*tasks)


if __name__ == "__main__":
    asyncio.run(main())
