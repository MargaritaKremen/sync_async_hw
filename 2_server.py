import asyncio


async def handle_client(reader, writer):
    # Отримую дані від клієнта
    data = await reader.read(1024)
    if data:
        # Декодую отримані дані
        numbers = data.decode().split(",")
        try:
            # Конвертую рядки в числа і отримую суму
            num1 = int(numbers[0])
            num2 = int(numbers[1])
            result = num1 + num2
            print("Результат обчислення на сервері: ", result)

            # Конвертую результат в рядок
            response = str(result).encode()

            # Відправляю результат клієнту
            writer.write(response)
            await writer.drain()
        except ValueError:
            print("Помилка при обробці даних від клієнта")

    # Закриваю з'єднання
    writer.close()


async def main():
    # Адреса та порт:
    server_address = await asyncio.start_server(handle_client, "0.0.0.0", 12345)
    async with server_address:
        await server_address.serve_forever()


if __name__ == "__main__":
    asyncio.run(main())
