# Завдання 1
# Створіть співпрограму, яка отримує контент із зазначених посилань і логує хід виконання в database,
# використовуючи стандартну бібліотеку requests, а потім проробіть те саме з бібліотекою aiohttp.
# Кроки, які мають бути залоговані: початок запиту до адреси X, відповідь для адреси X отримано зі
# статусом 200. Перевірте хід виконання програми на >3 ресурсах і перегляньте послідовність запису
# логів в обох варіантах і порівняйте результати. Для двох видів завдань використовуйте різні файли
# для логування, щоби порівняти отриманий результат.

import requests
import logging

logging.basicConfig(filename="log_for_sync.txt", level=logging.INFO)


def fetch_url(url, logger):
    logger.info(f"Starting reguest for {url}")
    response = requests.get(url)
    if response.status_code == 200:
        logger.info(f"Received response 200 for {url}")
    else:
        logger.warning(f"Received response {response.status_code} for {url}")


logger = logging.getLogger(__name__)

urls = [
    "http://www.testingmcafeesites.com/index.html",
    "https://github.com/",
    "https://www.jython.org",
    "https://google.com",
]


for url in urls:
    fetch_url(url, logger)
