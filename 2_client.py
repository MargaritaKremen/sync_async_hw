# Завдання 2
# Розробіть сокет-сервер на основі бібліотеки asyncio.

import asyncio


async def send_data():
    reader, writer = await asyncio.open_connection("127.0.0.1", 12345)

    # Введення двох чисел
    num1 = input("Введіть перше число: ")
    num2 = input("Введіть друге число: ")

    # Формування рядка через кому
    message = f"{num1},{num2}".encode()

    writer.write(message)

    await writer.drain()

    # Отримуємо та виводимо результат від сервера
    response = await reader.read(1024)
    print("Отримано від сервера: ", response.decode())

    writer.close()
    await writer.wait_closed()


async def main():
    await send_data()


if __name__ == "__main__":
    asyncio.run(main())
